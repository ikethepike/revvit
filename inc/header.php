<div id='page-dim'></div>

<div id='tease-zone'>
	<div id='tease'></div>
</div>

<?php include 'inc/sidemenu.php'; ?>


<div id='main-wrapper'>

	<header id='site-header' class='menu-inactive '>
		<nav>
			<div id='logo-wrapper'>
				<ul>
					
					<li id='main-logo'>
						<a href='<?php echo BASE_URL; ?>'>
							<h1 title='Go home'> <?php echo $site_title; ?> </h1>
						</a>
					</li>

					<li id='main-menu'>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class='hamburger'><g><path d="M3,18h18v-2H3V18z M3,13h18v-2H3V13z M3,6v2h18V6H3z"></path></g></svg>
					</li>
				</ul>
			</div>

			<div id='search-wrapper'>
				<form id='main-search' action="search.php" method="GET">
					<input type='text' name='search' id='search-field' required placeholder='Just start typing...'>
					<input type='submit' value=''>
				</form>
			</div>
		</nav>

	</header>
