<?php

class Upload{

	private $_db,
			$_dir,
			$_count,
			$_uploaded 	= array(),
			$_errors 	= null;

	// Initialize DB and save as private var

	function __construct(){
		$this->_db = DB::getInstance();

		$this->_dir = Config::get('uploads/dir');
	}	

	// Upload function to be run after file checks

	private function upload($file) {
		
		for($i = 0; $i < $this->_count; $i++){
			

			$file_name 	= $file['name'][$i];
			$file_parts = pathinfo($file_name);
			$extension 	= $file_parts['extension'];

			$new_name	= Hash::make(20) . "." . $extension;

			$destination = $this->_dir . $new_name;

			if(move_uploaded_file($file['tmp_name'][$i], $destination)){
				
				$url 	= (string) BASE_URL . "/" . $destination;
				$hash 	= Hash::make(64);


				$this->_uploaded[$i] = array(
							'url' 		=> $url,
							'og_name' 	=> $file['name'][$i],
							'hash'		=> $hash,
				);

				$data = array(
					'url'		=> $url,
					'mime_type'	=> $extension,
					'hash'		=> $hash,
				);


			$insert = $this->_db->insert('uploads', $data);

			print_r($insert);
			$false = false;
			
			if($false){
				$this->_errors[] = 'Insert failed';
				$this->_uploaded['success'] = false; 
				return false;
			}
		}

		$this->_uploaded['success'] = true; 
		return $this->_uploaded;
	}

	} // End upload


	// Image upload with mime type checks
	public function image($images, $resize = true){

		// Make sure that something was passed
		if(!empty($images['name'][0])){
			
			$this->_count = count($images['name']);

			// Loop through all images and check mime type

			for($i = 0; $i < $this->_count; $i++){

				// Get file type
				$file_name = $images['name'][$i];
				$extension = pathinfo($file_name, PATHINFO_EXTENSION);

				// Check against whitelist
				if(!in_array(strtolower($extension), Config::get('uploads/image_ext'))){
					$this->_errors[] = $file_name . " is a not an allowed file type."; 
				}
				
			}

			// If no errors check
			if(empty($this->_errors)){
				echo json_encode($this->upload($images));
				return true;
			} else {
				return json_encode($this->_errors);
				// Else return all errors
			}
		
		} // Empty check

		return false;
	}

	
	// Unfinished crop function

	// public static function crop($image){
	// 	$ext = pathinfo($image[], PATHINFO_EXTENSION);

	// }


	public static function errors(){
		return $this->_errors;
	}

	public static function successes(){
		return $this->_uploaded;
	}
}

