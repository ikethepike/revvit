<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 
?>

</head>
<body id='main-body' class='login-page'>

<?php include 'inc/header.php'; ?>

<div id="login-page-wrapper">
	
	<div id='login-wrapper'>
		<div class='wrap'>
			<h3> Login to Revvit </h3>
			<?php include 'modules/login.php'; ?>
		</div>
	</div>
	
</div>


<?php include 'inc/footer.php'; ?>