// Project Settings

var dir = ".";
	

// Date and Time

var currentdate = new Date(); 
var datetime = "Last Sync: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();	

// Dependencies

	var gulp = require('gulp'),
	uglify = require('gulp-uglify')
	autoprefixer = require('gulp-autoprefixer')
	sass = require('gulp-sass')
	gutil = require('gulp-util')
	pngmin = require('gulp-pngmin')
	svg2png = require('gulp-svg2png')
	size = require('gulp-size')
	notify = require("gulp-notify")
	imagemin = require('gulp-imagemin')
	pngquant = require('imagemin-pngquant')
	livereload = require('gulp-livereload')
	git = require('gulp-git')
	plumber = require('gulp-plumber');
	
//JS uglify
	gulp.task('compress', function() {
	  return gulp.src(dir + '/js/scripts.js')
	  	.pipe(plumber())
	    .pipe(uglify())
	    .pipe(plumber.stop())
	    .pipe(gulp.dest(dir + '/js/min/'))
	});

// CSS Autoprefixer
gulp.task('autoprefixer', function () {
    return gulp.src(dir + '/css/*.css')
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
    .pipe(gulp.dest(dir + '/css/'));
});


// PNG Minification
gulp.task('pngmin', function(){
	  gulp.src(dir + '/img/**/*.png')
	  .pipe(pngmin())
      .pipe(gulp.dest(dir + '/img'));

});

// Image optimization

gulp.task('img', function () {
    return gulp.src(dir + '/img/unoptimized/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [
	            {removeViewBox: false},
	            { removeUselessStrokeAndFill: false },  
	            { removeEmptyAttrs: true }    
            ],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(dir + '/img/'));
});


//SCSS Processing
gulp.task('sass', function () {
    gulp.src(dir + '/sass/**/*.scss')
	  	.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(sass({outputStyle: 'compressed'}))
        .pipe(plumber.stop())
    	.pipe(autoprefixer())
        .pipe(gulp.dest(dir + '/css'));
        console.log('SASS compile done');
});

// SVG to PNG
gulp.task('svg2png', function () {
    gulp.src(dir+'/img/**/*.svg')
        .pipe(svg2png())
        .pipe(gulp.dest('./build'));
});

// Project Size
gulp.task('size', function () {
    var s = size();
    return gulp.src('fixture.js')
        .pipe(s)
        .pipe(gulp.dest('dist'))
        .pipe(notify({
            onLast: true,
            message: function () {
                return 'Total size ' + s.prettySize;
            }
        }));
});

// Git 

gulp.task('add', function(){
  return gulp.src('.')
    .pipe(git.add());
});

gulp.task('commit', function(){
  return gulp.src('*')
    .pipe(git.commit(['build'+ datetime, 'Automatic build compilation']));
});

gulp.task('push', function(){
  git.push('origin', 'master', function (err) {
    if (err) throw err;
  });
});

gulp.task('pull', function(){
  git.pull('origin', 'master', {args: '--rebase'}, function (err) {
    if (err) throw err;
  });
});


// Watch Task
gulp.task('watch', function(){
	gulp.watch(dir+'/img/unoptimized/*', ['img']);
	gulp.watch(dir+'/js/*.js', ['compress']);
	gulp.watch(dir+'/sass/**/*.scss', ['sass']);

});

gulp.task('live', function(){
	livereload.listen();
	gulp.watch(dir+'/sass/**/*.scss', ['sass']);
	gulp.watch(dir+'/css/**/*.css', ['autoprefixer']);
});


gulp.task('build', function(){
	gulp.start('compress', 'sass', 'autoprefixer', 'img', 'add', 'push');

});

gulp.task('default', function() {
    gulp.start('watch');
});