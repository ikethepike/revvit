<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 
?>

	<title> Register </title>

</head>
<body style='background: url(img/glasgow.jpg) no-repeat center; background-size:cover;' id='main-body'>

<div id='register-canvas'></div>
<?php include 'inc/header.php'; ?>


	<div id='register-page'>
		<header class='page-heading'>
			<h2> Register </h2>
			<span class='description'> Register now to become a member of Revvit, where you can add and edit cities. </span>
		</header>

		<?php include('modules/register.php'); ?>
	</div>





<?php include 'inc/footer.php'; ?>