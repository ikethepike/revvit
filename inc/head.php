<?php
	// General Site Vars and functions
	require "core.php";

?>

<!DOCTYPE html>
<html id='main-html' lang='en'>
<head>

	<!-- Single page site 
	<title> <?php // echo $page_title; ?> </title>
	-->


	<link rel="stylesheet" type="text/css" href="css/base.css">
	<script type="text/javascript" src='js/scripts.js'></script>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name='description' content="<?php echo $GLOBALS['site_description']; ?>">
	
	<script src="https://use.typekit.net/clw5vaw.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<?php $user = new User(); ?> 
