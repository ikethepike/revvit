<div id='city-grid' class='city-grid-stowed'>
	<nav>
		<div class='close' id='city-grid-close'>
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="560px" height="560px" viewBox="0 0 560 560" enable-background="new 0 0 560 560" xml:space="preserve">
				<path fill="none" stroke-width="73" stroke-miterlimit="10" d="M530,530L30,30 M530,30L30,530"/>
			</svg>
		</div>
		
		<div class='inner-wrap'>
			
			<header id='city-grid-header'>
				<h2> Cities </h2>
				<span id='city-description'>
					Here, you will be able to see all the cities available on Revvit. You will even be able to add new cities. 
				</span>
			</header>

			<ul> 
				<?php 
					$DB = DB::getInstance();

					foreach($DB->table('posts') as $index => $post ) : ?>
						<li>
							<a href='<?php echo BASE_URL . "/post?id=" . $post['id']; ?>'>
								<h3> <?php echo $post['title']; ?> </h3>
								<div class='cover' style='background: url(<?php echo $post['featured_image']; ?>) no-repeat center; background-size:cover'></div>
							</a>
						</li>
					<?php endforeach; ?>


				<li id='new-city-block'>
					<?php echo ($user->isLoggedIn()) ? "<a href='". BASE_URL ."/create.php' >" : "<a href='". BASE_URL ."/login.php' >" ;?>
					<h3> Create New </h3>
					<div class='cover'></div>
					</a>
				</li>
			</ul>
		</div>
	</nav>
</div>