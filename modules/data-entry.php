<div class="input-wrapper">
	<div id='graph-input'>
		<h3 class="heading">Graph Input</h3>
		<div class="column"><label for="x-input">X value</label><input type="number" name="x-input" id="x-input" class="wrapper"></div>
		<div class="column"><label for="y-input">Y value</label><input type="number" name="y-input" id="y-input" class="wrapper"></div>
		
		<div class='button-wrapper'>
			<div id="add-values-button">
				Add Values
			</div>
			<div id='remove-values-button'>
				Reset Values
			</div>
		</div>

		<table id="table-records">
			<?php
				if(isset($x_values)):
					
					$decoded_x = json_decode($x_values);
					$decoded_y = json_decode($y_values);

					if($edit && !empty($decoded_x) ){


						foreach ($decoded_x as $index => $x_value) {
							echo "<tr><td>{$x_value}</td><td>{$decoded_y[$index]}</td></tr>";
						}
					}

				endif; 

			?>
		</table>

		<input type='hidden' id='x-values' name='x-values' value="<?php echo ($edit) ? $x_values : Input::get('x-values'); ?>">
		<input type='hidden' id='y-values' name='y-values' value="<?php echo ($edit) ? $y_values : Input::get('x-values'); ?>">

	</div>
</div>