
<?php 
	if($user->isLoggedIn()):
		Redirect::to('index.php');
	else:
	

	if(Input::exists() && !empty($_POST)) {
		if( Token::check(Input::get('login-token')) ) {
			$validate 	= new Validate();
			$validation = $validate->check($_POST, array(
				'login-username'	=> array('required' => true, 'item' => 'A username'),
				'login-password'	=> array('required' => true, 'item' => 'A password')
			));


			if($validation->passed()) {
				$user 		= new User();
				$remember 	= (Input::get('remember') === 'on') ? true : false;
				$login 		= $user->login(Input::get('login-username'), Input::get('login-password'), $remember); 

				if($login){
					Session::flash('login', 'login-blip');
					Redirect::to('index.php');
				} else {
					echo "<div class='site-message-wrapper errors-1'>";
					echo "<span class='error-message'>Incorrect login details</span>";
					echo "</div>";
				}

			} else {
				$number = count($validation->errors());
				echo "<div class='site-message-wrapper errors-{$number}'>";
					
					foreach($validation->errors() as $error){
						echo $error;
					}

				echo '</div>';
			}

		}
	}

	endif; // Logged in check

?>


<div id='login-form'>
	<form method="POST">
		<div class='field'>
			<label for='login-username'> Username </label>
			<input type='text' name='login-username' id='login-username' placeholder='Username'>
		</div>

		<div class='field'>
			<label for='login-password'> Password </label>
			<input type='password' name='login-password' id='login-password' placeholder='Password'>
		</div>

		<div class="field">
			<label for='remember'>
				<input type='checkbox' name="remember" id='remember'>
				Remember me?
			</label>
		</div>

		<div class='submit-wrapper'>
			<input type='hidden' name='login-token' value='<?php echo Token::generate(); ?>'>
			<input type='submit' class='submit-button' value='Log in'>
		</div>

		<div class='submit-wrapper'>
			<a class='submit-button' href='<?php echo BASE_URL . "/register.php"; ?>'> Register </a>
		</div>

	</form>
</div>