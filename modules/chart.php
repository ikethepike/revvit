
<?php 

	$xValues = json_decode($article->x_values);
	$yValues = json_decode($article->y_values);

	// Make sure that content exists
	if(!empty($xValues[0])): 


	$max_x	 = max($xValues);
	$countX  = count($xValues);
	$max_y	 = max($yValues); 
	$min_x 	 = min($xValues);
	$min_y 	 = min($yValues);

?>
<div id="chart-wrapper">
	<header class="chart-header">
		<h2 id='post-chart' class='post-heading'>Total arts expenditure by <?php echo $article->title; ?> council</h2>
	</header>
	<canvas id="chart" width="1630" height="930"></canvas> 
</div>

<script type="text/javascript">
	
	var c = document.getElementById("chart"),
	 	ctx = c.getContext("2d"),
		grd = ctx.createLinearGradient(0,900,1600,0);


		ctx.moveTo(0,<?php echo ($yValues[0] / $max_y ) * 900; ?>);
		ctx.font = "30px Arial";
		ctx.fill();
		ctx.fillStyle = "#252525"; //Black


		<?php foreach($xValues as $index => $value): 

				$pos_x = (1600 / ($countX) ) * $index;
				$pos_y = ($yValues[$index] / $max_y ) * 900;
				$dis_y = $pos_y / 1.05;
				$dis_x = $pos_x;


			?>
			ctx.lineTo(<?php echo $pos_x .  "," . $pos_y  ?>);
			ctx.arc(<?php echo $pos_x ."," . $pos_y; ?>, 5, 0, 2 * Math.PI, true);
			ctx.fillText(<?php echo "'{$value} | £ {$yValues[$index]}'"; ?>, <?php echo $dis_x ."," . $dis_y; ?> );
		<?php endforeach; ?>
	 	ctx.stroke();
</script>




<?php endif; ?>

