	<main>
		<div class='post-masthead-wrapper'>
			<div class="post-masthead-title">
				<h2><?php echo escape($article->title); ?></h2>
			</div>
			<div class="post-masthead-images">
				<div class='main-image'>
					<img src="<?php echo escape($article->featured_image); ?>" alt='image of <?php echo $article->title; ?>'/>
				</div>
				
				<div id="masthead-side-gutter"></div>

			</div>
		</div>
	</main>

	
	<div class='information-page' id='post-body'>
		<div class='post-wrapper'>

			<div class="post-introduction">
				<h3 id='post-introduction' class='post-heading'> Introduction </h3>
				<?php echo $article->introduction; ?>
			</div>
			
			<?php include 'chart.php'; ?>

			<div class="post-body">
				<h3 id='post-institutions' class='post-heading'> Institutions </h3>
				<?php echo $article->content; ?>
			</div>

		</div>

		<div class="gutter">
			<div id='nav-widget'>
				<h4 class="post-heading">Navigation</h4>
				<aside class="document-sections">
					<ol>
						<li>
							<a href='#post-introduction'>
								Introduction
							</a>

						</li>
						<li>
							<a href='#post-institutions'>
								Institutions
							</a>
						</li>
						<li>
							<a href='#post-chart'>
								Council Income
							</a>
						</li>
						<li>
							<a href='#post-community'>
								Community
							</a>
						</li>
					</ol>
				</aside>
			</div>
		</div>
	</div>
