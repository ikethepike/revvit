
<div id='side-menu' class='side-stowed'>

		<?php 

			if($user->isLoggedIn()): ?>
				
				<div id="side-avatar-wrapper">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 width="414.667px" height="410.667px" viewBox="0 0 414.667 410.667" enable-background="new 0 0 414.667 410.667"
						 xml:space="preserve">
					<g>
						<circle fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" cx="208.089" cy="205.718" r="194.35"></circle>
						<path fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" d="M353.313,334.859
							c0,0-44.985-105.085-144.979-105.085c-109.61,0-145.469,105.085-145.469,105.085"></path>
						<circle fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" cx="208.089" cy="156.951" r="62.147"></circle>
					</g>
					</svg>	
				</div>
				
				<a href='<?php echo BASE_URL; ?>/profile.php'>
					<div id='side-name-wrapper'>
						<?php $greeting = array('Howdy', 'Hi', 'Hello', 'Yo', 'Aloha', 'Hey'); 

						$number = rand(0, count($greeting) - 1);

						echo $greeting[$number] . ", " . $user->data()->username . "!";

						?>
					</div>
				</a>

				<ul id='user-options'>
					<li>
						<a href='<?php echo BASE_URL; ?>/create.php'> 
							Create Post
						</a>
					</li>

					<li>
						<a href='<?php echo BASE_URL . '/edit-profile.php'; ?>'>
							Edit User
						</a>
					</li>
					
					<li>
						<a href='logout.php'> Logout </a>
					</li>

				</ul>

			<?php else:  ?>
			<ul>
				<li>
					<a href='<?php echo BASE_URL; ?>/register.php'> 
						Register
						<div class='target'></div>
					 </a>
				</li>

				<li> 
					<a href='<?php echo BASE_URL; ?>/login.php'> 
						Login
						<div class='target'></div>
					 </a>
				</li>
			</ul>
		<?php endif; ?>
</div>