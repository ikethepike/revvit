-- MySQL dump 10.16  Distrib 10.1.8-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: revvit
-- ------------------------------------------------------
-- Server version	10.1.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(400) NOT NULL,
  `introduction` text NOT NULL,
  `x_values` varchar(250) NOT NULL,
  `y_values` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `revisions` text NOT NULL,
  `featured_image` varchar(600) NOT NULL,
  `hash` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (38,2,'Glasgow','<p>Glasgow has a vibrant art scene, recently hosting the prestigious <a title=\"t\" href=\"http://www.tramway.org/turner-prize/Pages/default.aspx\">Turner prize</a>. The City itself boasts the Glasgow School of Art designed by <a href=\"http://www.gsa.ac.uk/visit-gsa/mackintosh-building-tours/charles-rennie-mackintosh/\">Charles Rennie Mackintosh</a>, and a wide variety of different venues ranging from traditional art galleries such as the <a href=\"http://www.gla.ac.uk/hunterian/collections/permanentdisplays/hunterianartgallery/\">Hunterian Art Gallery</a> to smaller more experimental spaces like the monthly exhibitions and performances staged at the <a href=\"http://www.cca-glasgow.com/programme#exhibitions\">CCA (Center for Creative Arts)</a>.&nbsp;<br /><br />Formerly an industrial city, Glasgow experienced a<a href=\"http://www.scotland.org/features/glasgow-reinvented-the-regeneration-of-glasgow-and-the-river-clyde/\"> steep decline in the 20th century</a> as the vital boat building and shipping industries fell into decline. Yet, following extensive urban renewal programs in the 1990s and a resumed focus on the arts, Glasgow has experienced somewhat of a cultural efflorecence during the past decade. The city now retreads its former role as a major cultural center in the UK, supporting and producing internationally recognizable art. A sense of pride that has not existed since the Glasgow Boys has been recaptured, evidenced in the <a href=\"https://www.nationalgalleries.org/media/_file/press_releases/GENERATION_PRESS_RELEASE_07.11_.13_final_.pdf\">Generations show</a>&nbsp;(2014)&nbsp;which triumphantly touted Scottish artists such as&nbsp;David Shrigley, Charlotte Prodger, Simon Starling, Zo&euml; Walker &amp; Neil Bromwich, Cara Tolmie and Richard Wright.&nbsp;</p>\r\n<p>Indeed many artists are turning to Glasgow, <a href=\"http://www.theguardian.com/artanddesign/2014/jun/26/generation-review-scottish-contemporary-art\">fleeing the highly structuralized and commercial art market that has grown up around London.</a>&nbsp;To many, Glasgow offers a more independent art scene, and a more diverse audience - <a href=\"http://www.glasgowlife.org.uk/museums/about-glasgow-museums/Pages/home.aspx\">aided by free admissions</a>&nbsp;and a wide variety of different heritage institutions.&nbsp;</p>','[\"2009\",\"2010\",\"2011\",\"2012\",\"2013\",\"2014\",\"2015\"]','[\"95459000\",\"104728000\",\"113112000\",\"116134000\",\"118332000\",\"130186000\",\"113959000\"]','<p>Glasgow is host to a wide variety of different galleries, museums and venues. Some of these include the popular<a href=\"http://www.glasgowlife.org.uk/museums/GoMA/about/Pages/default.aspx\"> GOMA (Gallery of Modern Art)</a> which ranks as the most popular gallery in Scotland, the <a href=\"http://www.tramway.org/turner-prize/Pages/default.aspx\">Tramway (Host to 2015\'s Turner Prize)</a>, and more specialized galleries such as the <a href=\"http://www.glasgowlife.org.uk/museums/burrell-collection/Pages/default.aspx\">Burrell Collection</a>, which houses the varied collection of William Burrell.&nbsp;</p>\r\n<h2>GOMA</h2>\r\n<p><img src=\"https://upload.wikimedia.org/wikipedia/commons/9/96/Gallery_of_Modern_Art_%28GoMA%29_and_Duke_of_Wellington_Traffic_Cone%2C_Glasgow%2C_Scotland_12281772004.jpg\" alt=\"GOMA\" width=\"4594\" height=\"3063\" /></p>\r\n<p>The Gallery of Modern Art is perhaps the most influential modern art gallery in Scotland, as it is the most visited contemporary art museum outside of London. (P. 4, Katie Bruce et al., <em>Towards an Engaged Gallery</em>, 2007, Culture &amp; Sport Glasgow) Its image, a neo-classical building with towering doric pillars standing behind a cone-bearing statue of Duke Wellington has become an image indelibly linked with Glasgow.&nbsp;</p>\r\n<blockquote>\r\n<p>SHRIEK WHEN THE PAIN HITS DURING INTERROGATION. REACH INTO THE DARK AGES TO FIND A SOUND THAT IS LIQUID HORROR, A SOUND OF THE BRINK WHERE MAN STOPS AND THE BEAST AND NAMELESS CRUEL FORCES BEGIN. SCREAM WHEN YOUR LIFE IS THREATENED. FORM A NOISE SO TRUE THAT YOUR TORMENTOR RECOGNIZES IT AS A VOICE THAT LIVES IN HIS OWN THROAT.</p>\r\n<p>(Excerpt from Inflammatory Essays (1979-82) &copy; Jenny Holzer 2007 featured at Sanctuary Exhibition 2003 at GOMA)</p>\r\n</blockquote>\r\n<p>&nbsp;The museum also embodies part of the progressive nature of the city, channeling issues of human rights, violence against women and sectarianism into tangible exhibitions and citywide social justice programs. (ibid) This dedication to social justice is enshrined within the organization itself where it addresses acquisitions, preservation and social justice as equal. (P. 14, ibid)</p>\r\n<p>This has helped raise the national and international profile of the museum and resulted in several cooperations, welcoming many artists of worldwide renown.&nbsp;</p>\r\n<h2>The Burrell Collection</h2>\r\n<p><img src=\"http://www.glasgowlife.org.uk/museums/burrell-collection/PublishingImages/Burrell.png\" alt=\"the burrell collection\" width=\"700\" height=\"250\" /></p>\r\n<p>The Burrell collection was donated by Lady and Sir Burrell in the 1944. The collection boasts an impressive range of items stemming from&nbsp;the ancient civilizations of Iraq, Iran (Mesopotamia, Assyria, Kassite, Sumerian) to Egypt, Italy and Greece. Included in these works are some delicately crafted&nbsp;alabaster statuettes and reliefs as well as an array of different items of pottery.&nbsp;</p>\r\n<blockquote>\r\n<p><em>\"Let there be no mistake about it: in all history, no municipality has ever received from one of its native sons a gift of such minificience as that which, in 1944, the City of Glasgow accepted from Sir William and Lady Burrell.\" </em>(P. 7, The Burrell Collection, 1987, William Collins Sons and Company LTD)<br /><br /></p>\r\n</blockquote>\r\n<p>The collection also encompasses a vast array of works stemming from Eastern Asia. Although the basis for William Burrell\'s interest in the works remains a mystery, the importance he placed on acquiring is undisputable. These works comprise roughly 25% of the broader collection, and represent one of the largest collection of East Asian art in Scotland. &nbsp;</p>\r\n<p><code><iframe src=\"https://www.youtube.com/embed/bO4sOyKTGDU\" width=\"560\" height=\"315\" frameborder=\"0\" allowfullscreen=\"\"></iframe></code></p>\r\n<p>The Burrell collection also incorporates a selection of French, Flemish and Dutch art from the 19th and 20th century, illustrating the diversity in the collection.<br /><br /></p>','2016-03-17 21:56:27','','http://localhost/revvit/uploads/fdd9dbaea75f9524003628ed2495446688be4a88.jpg',''),(39,2,'Newcastle','<p>Newcastle houses a distinct and unique art scene. Not merely does it contain&nbsp;some of the most important northern art institutions such as the Baltic (host of the 2011 Turner prize), but also a wide array of different festivals such as Vamos, Green Festival, Ouseburn and the popular Evolution festival. Newcastle prides itself on its indepedenent art scene that has <a href=\"http://www.theguardian.com/culture-professionals-network/culture-professionals-blog/2013/mar/08/newcastle-arts-cuts-culture-fund\">weathered some of the hardest austerity in the country.</a>&nbsp;The city home to a diverse population of 1,650,000 presents an inviting city for artists and institutions alike, but with dwindling public funding and a fairly saturated art market, Newcastle might provide a challenging point of entry.&nbsp;</p>','[\"2013\",\"2014\",\"2015\"]','[\"1931281\",\"1660249\",\"2159167\"]','<p>The art venues of Newcastle are many, varied and storied, with iconic venues such as the Baltic looking out over the river Tyne next to both of the citiy\'s famous bridges (Tyne Bridge and the Gateshead Millenium&nbsp;Bridge).&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h2>The Baltic</h2>\r\n<p><img src=\"http://www.balticmill.com/system/PostAttribute/images/000/004/588/large/_DSC0566a.jpg\" alt=\"Image of the Baltic\" width=\"2331\" height=\"1550\" /></p>\r\n<blockquote>\r\n<p><em>The fog on the Tyne is all mine<br /></em><em>The fog on the Tyne is all mine, all mine<br /></em><em>The fog on the Tyne is all mine.&nbsp;</em><br /><em>(Lindisfarne, Fog On the Tyne,&nbsp;1971)</em></p>\r\n</blockquote>\r\n<p>The Baltic represents Newcastle in upswing, it is the repurposing of the dilapidated tyneside industry into a internationally famous modern art gallery. It draws on the storied past of the city and melds it with modern culture, embodying the collective goals for the city.&nbsp;More than just an art space, the Baltic also serves as a broader cultural venue in the city allowing for short-term&nbsp;performances, installations and events. &nbsp;Yet, the Baltic one of the most influential and important institutions&nbsp;of modern art outside of London, something that is reflected in its visits which amounted to <a href=\"http://www.balticmill.com/about/baltic-history\">6 million in 2015. </a></p>\r\n<p>&nbsp;</p>\r\n<h2>The Biscuit Factory</h2>\r\n<p><img src=\"http://i4.chroniclelive.co.uk/incoming/article1766925.ece/ALTERNATES/s615/factory.jpg\" alt=\"The Biscuit Factory\" width=\"615\" height=\"409\" /></p>\r\n<p>The Biscuit Factory is one of Northern England\'s largest indepedent art, craft and design galleries. The Biscuit Factory is not publicly funded, and instead relies on commissions from art sales, as of such it lowers the barrier for entry for new or establishing artists. The space is beautifully furnished within an old Victorian warehouse in the cultural district.&nbsp;</p>','2016-03-17 23:15:15','','http://localhost/revvit/uploads/bdb0242752c0c2f8aaaf19e0b640b91e59af8fcc.jpg','');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `mime_type` varchar(10) NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hash` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (50,'http://localhost/revvit/uploads/fdd9dbaea75f9524003628ed2495446688be4a88.jpg','jpg','2016-03-17 21:56:25','c46dfae30b4fd6cc563cc8800c1d0f5b8510b40f542fa3aba6a4a53d70cdd08f49be828908a3d6dd32bc9357f75aaa05aa828a0a016ea8695ad5d41e10d492e8'),(51,'http://localhost/revvit/uploads/bdb0242752c0c2f8aaaf19e0b640b91e59af8fcc.jpg','jpg','2016-03-17 23:15:12','e1283583d9567d784ac2ffe68e43ec8caf9c2e71cf312a1f9c81470b1adf331e78a62b6f581e620ba2709dba5060d1a2c3d5c5e7c6fcfde5d2a69d2d5273ca10'),(52,'http://localhost/revvit/uploads/8fee003bea40cc53cd8523e4a240b1e251477390.png','png','2016-03-18 14:47:34','5ea4f02ee06ef8ccb4f6a4213f24e4680d7ae162e28410b30dd97d4c4b0d817302846292e93b2f7d05e408023eb25ae165855141bdb081d05a1ca07c16cbf111'),(53,'http://localhost/revvit/uploads/98838c2415966e0677aa19f4b89b317c89765f1b.png','png','2016-03-18 14:47:39','451c490c596cb81931b6ca0f9371e28c6ebe6d66b7ccc164d6fea95d61434d4a57a9ac6dfd8837d66f4236b43abddb8dbe2e7ae9d5f1b1f6d2e8a7ce8eaf9450'),(54,'http://localhost/revvit/uploads/7faf502804d7c66990b9ecfd3b8857871000bccf.jpg','jpg','2016-03-18 14:47:44','64dc884bff42cbba3a37babab768b31ff7e71887c64c50cb0951fc43694a46959fc7f52b3ddede17aba3871ca93cb8c4ac0dc905202f2a30a4d12af992c4e1b3'),(55,'http://localhost/revvit/uploads/1ff4d6246f21d934acaeb2d1c62f91d5bcbe8f8c.png','png','2016-03-18 14:48:05','104d67c6607692374341b1ae741180cdce4ed16ef357e86821ca377960653e3264e6391caf5f4becf0cb4d4c6b971a399641fd2cc62919dbc7d2027443d72977'),(56,'http://localhost/revvit/uploads/0897b49614d500220db2f72c093fd5486f809f0c.jpg','jpg','2016-03-18 20:55:08','b41afe638d8fa0569de784c96ddeefaa5fa06929fe734716c779de9bf9d76b48f2cc2480987aaaa05e0d46165c030aa946b9883215bc48d74206597ed300e754'),(57,'http://localhost/revvit/uploads/96b154fdfe1422b61d022d2b1659886c4658c953.jpg','jpg','2016-03-18 21:57:10','253acea1ccbc05921eca99a519a1a492f8f282bbaef97b4b100bef879c066172268a449bcd52812344cab87a7cb6ac90652434cc0273b3eec904a0bdd1336d03');
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `hash` varchar(72) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_sessions`
--

LOCK TABLES `user_sessions` WRITE;
/*!40000 ALTER TABLE `user_sessions` DISABLE KEYS */;
INSERT INTO `user_sessions` VALUES (2,0,'50fde8a095211564dac6857c315419d4dede6eb24785292f2567521c3e434984');
/*!40000 ALTER TABLE `user_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(111) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(32) NOT NULL,
  `activation` varchar(32) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'ikethepike','$2y$10$Z1xOKnuU5sabMVy.t42hzOPU10hPAP2qAqoaMSk0DeV/JAbMTPc5S','ikethepike@outlook.com','5b547c3ff05b8f1ac40212316a72509e','2016-03-13 21:59:21'),(3,'jenny','Barnabush0pkins','ike_easy@hotmail.com','9f61408e3afb633e50cdf1b20de6f466','2016-03-24 15:23:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-14 12:43:15
