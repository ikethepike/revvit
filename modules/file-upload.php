<div id="uploads"></div>

<div class="dropzone" id="dropzone" style='<?php echo ($edit) ? "background: url($feat_image) no-repeat center; background-size: cover;" : Input::get('image-link'); ?>'>

	<span> Drag files here to upload </span>
	
	<div id='dropzone-dim'></div>

</div>

<!-- Random hash correlated against DB to set featured image, image link solely used to maintain placeholder img -->
<input type="hidden" name="image-hash" id="image-hash" value='<?php echo Input::get('image-hash'); ?>'/>
<input type="hidden" name='image-link' id='image-link' value='<?php echo Input::get('image-link'); ?>'>

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(ev) {


	var dropzone 		= document.getElementById('dropzone'),
		dropzone_dim 	= document.getElementById('dropzone-bg'),
		image_hash		= document.getElementById('image-hash'),
		image_link		= document.getElementById('image-link');

	dropzone.ondragover = function(){

		this.className = 'dropzone dragover';
		return false;
	}

	dropzone.ondragleave = function(){
		this.className = 'dropzone';
		return false;
	}

	dropzone.ondrop	= function(e){
		e.preventDefault();

		this.className = 'dropzone';

		upload(e.dataTransfer.files);
		
	}


	function upload(files) {
		var formData 	= new FormData(),
			xhr		 	= new XMLHttpRequest(),
			i;

			for(i = 0; i < files.length; i++) {
				formData.append('file[]', files[i]);
			}

			xhr.onload = function(){

				console.log(this.responseText);

				var data = JSON.parse(this.responseText);

				if(data.success){
					dropzone.style.background 	= "url('" + data[0]['url'] + "') no-repeat center";
					image_link.value			= "background: url(" + data[0]['url'] + ") no-repeat center";
					image_hash.value			= data[0]['hash'];
				} else {
					dropzone.style.border	= '1px dashed red';
				}
			}

			xhr.open('post', 'uploadhandler.php');
			xhr.send(formData);

	}	

});
</script>