document.addEventListener("DOMContentLoaded", function(ev) {
	
	var hamburger			= document.getElementById('main-menu'),
		sideMenu			= document.getElementById('side-menu'),
		header 				= document.getElementById('site-header'),
		cityNav				= document.getElementById('city-grid'),
		search 				= document.getElementById('search-field'),
		searchOverlay		= document.getElementById('search-overlay'),
		overlayInput		= document.getElementById('overlay-search-box'),
		tease				= document.getElementById('tease'),
		teaseZone			= document.getElementById('tease-zone'),
		cityGrid 			= document.getElementById('city-grid'),
		cityGridClose 		= document.getElementById('city-grid-close');
		closeOverlay 		= document.getElementById('close-overlay'),
		mainBody 			= document.getElementById('main-body'),
		inputs				= document.getElementsByTagName('input');

	hamburger.addEventListener('click', function() {
		header.classList.toggle('menu-active');
		sideMenu.classList.toggle('side-active');
	});

	// Open City Grid

	tease.addEventListener('click', function(){
		cityGrid.className += " city-grid-active";
		mainBody.className = "overflow";
		open = true;
	});

	// Close City Grid

	cityGridClose.addEventListener('click', function(){
		cityGrid.className = cityGrid.className.replace( /(?:^|\s)city-grid-active(?!\S)/g , '' );
		mainBody.className = "";
		open = false;
	});

	// Close search overlay

	closeOverlay.addEventListener('click', function(){
		searchOverlay.className = searchOverlay.className.replace( /(?:^|\s)overlay-active(?!\S)/g , '' );
		open = false;
	});

	// Make sure that the search input does not fire the search overlay


	var typing = false;


	for(var i = 0; i < inputs.length; i++){
		inputs[i].addEventListener('focus', function(){
			typing = true;
		});

		inputs[i].addEventListener('blur', function(){
			typing = false;
		});

	}



	// Type anywhere

	open = false

	document.onkeypress = function(ev){


		var charCode	= ev.keyCode,
			character	= String.fromCharCode(charCode),
			current 	= overlayInput.value;		


		if( ( charCode <= 90 && charCode >= 48 && !typing ) || (charCode >= 97 && charCode <= 122 && !typing) ){
			
			overlayInput.focus();

			if(!open){
				searchOverlay.classList.toggle('overlay-active');
				open = true;
				mainBody.className	= 'overflow';
			} 
		}
	}


	document.onkeydown = function(evt) {
	    evt = evt || window.event;
	    if (evt.keyCode == 27 && open) {
			searchOverlay.className = searchOverlay.className.replace( /(?:^|\s)overlay-active(?!\S)/g , '' );
			overlayInput.blur();
			open = false;
			mainBody.className	= '';
	    }
	};

	var xInput	= document.getElementById('x-input'),
		yInput	= document.getElementById('y-input'),
		addVal	= document.getElementById('add-values-button'),
		remVal 	= document.getElementById('remove-values-button'),
		table	= document.getElementById('table-records'),
		xValues = document.getElementById('x-values'),
		yValues = document.getElementById('y-values'),
		xArray 	= [],
		yArray 	= [];


	if(addVal !== null){

	addVal.addEventListener('click', function(){
		
		xValue = xInput.value;
		yValue = yInput.value;

		if(xValue.length > 0 && yValue.length > 0)	{

			xArray.push(xValue);
			yArray.push(yValue);

			table.innerHTML = table.innerHTML + "<tr><td>" + xValue + "</td><td>" + yValue + "</td></tr>";

			xValues.value = JSON.stringify(xArray);
			yValues.value = JSON.stringify(yArray);

			xInput.value  = '';
			yInput.value  = '';

		} else {
			alert('Faulty data');
		}



	}); // End Add Val function

	remVal.addEventListener('click', function(){
		table.innerHTML = '';
		xValues.value 	= '';
		yValues.value 	= '';
	});



} // Add Val Check

}); // document ready end






// Load function

function load(url, callback) {
	var xhr;

	if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
	else {
		var versions = ["MSXML2.XmlHttp.5.0", 
			 	"MSXML2.XmlHttp.4.0",
			 	"MSXML2.XmlHttp.3.0", 
			 	"MSXML2.XmlHttp.2.0",
			 	"Microsoft.XmlHttp"]

		for(var i = 0, len = versions.length; i < len; i++) {
		try {
			xhr = new ActiveXObject(versions[i]);
			break;
		}
			catch(e){}
		} 
	}
		
	xhr.onreadystatechange = ensureReadiness;
		
	function ensureReadiness() {
		if(xhr.readyState < 4) {
			return;
		}
			
		if(xhr.status !== 200) {
			return;
		}


		if(xhr.readyState === 4) {
			callback(xhr);
		}			
	}
		
	xhr.open('GET', url, true);
	xhr.send('');
}
