<?php

class Validate{
	private $_passed = false,
			$_errors = array(),
			$_db = null;

	public function __construct() {
		$this->_db = DB::getInstance();
	}


	// Check inputs
	public function check($source, $items = array()){
		foreach($items as $item => $rules){

			$name 	= $rules['item'];

			if(!empty($rules['table'])){
				$table 	= $rules['table'];
			} else {
				$table = 'users';
			}

			foreach($rules as $rule => $rule_value){

				$value 		= trim($source[$item]);


				if($rule === 'required' && empty($value)){
					$this -> addError("<span class='error-message'>{$name} is required</span>");
				} else if(!empty($value)){
					switch($rule){
						case "min":
							if(strlen($value)< $rule_value){
								$this->addError("<span class='error-message'>{$name} must be at least {$rule_value} characters.</span>");
							}
						break;
						case "max":
							if(strlen($value)> $rule_value){
								$this->addError("<span class='error-message'>{$name} must be a maximum of {$rule_value} characters.</span>");
							}
						break;

						case 'unique':

						    $check = $this->_db->get($table, array($item, '=', $value));

							if($check->count()) {
								$this->addError("<span class='error-message'> Sorry, but <span class='bold'>{$value}</span> is already registered.</span>");
							}

						break;
						case "matches":
						if ($value != $source[$rule_value]){
							$this->addError("<span class='error-message'>{$name} must match {$item}!</span>");
						}
						break;

						case "alpha":
						if(is_numeric($value)){
							$this->addError("<span class='error-message'>{$name} cannot be only numbers</span>");
						}
						break;

					}
				}
			}
		}

	if(empty($this ->_errors)){
		$this ->_passed = true;
	}
		return $this;
	}

	private function addError($error){
		$this ->_errors[] = $error;
	}

	public function errors(){
		return $this ->_errors;
	}

	public function passed(){
		return $this->_passed;
	}
}