<?php
	

	// Define Site Vars
	
	define('BASE_URL', "http://localhost/revvit");

	session_start();

	$GLOBALS['config'] = array(
		'mysql' => array(
			'host' 		=> '127.0.0.1',
			'user' 		=> 'root',
			'password'  => '',
			'db'		=> 'revvit'
			),
		'cookie' => array(
			'cookie_name' 	=> 'r_hash',
			'cookie_expiry' =>  2678400 // Month
		),
		'session' => array(
			'session_name' 	=> 'user',
			'token_id'		=> 'token'
		),
		'uploads' => array(
			'dir'			=> "uploads/",
			'image_ext'		=> array('jpg', 'png', 'gif', 'bmp', 'svg', 'jpeg'),
			'file_ext'		=> array('txt', 'mp4'),	
		),
	);

	// Autoloading Class files

	spl_autoload_register( function($class){
		require_once 'classes/'. $class . '.php';
	});

	require_once 'functions/sanitize.php';


	// Checking for cookie

	$cookie = Config::get('cookie/cookie_name');


	if(Cookie::exists($cookie) && !Session::exists(Config::get('session/session_name')) ){
		$hash = Cookie::get($cookie);

		$check 	= DB::getInstance()->get('user_sessions', array('hash', '=', $hash ));

		if(!empty($check)) {

			$user = new User($check->first()->user_id );
			$user->login();
		}
	}