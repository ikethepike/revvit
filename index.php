

<?php
	// Instantiate classes and functions
	require_once 'core/init.php';
?>


<?php include 'inc/head.php'; ?>
	

	<title> <?php echo $site_title; if(isset($subpage_title)){ echo " | " . $subpage_title; } ?> </title>
</head>
<body id='main-body'>

<?php
	if( Session::exists('success')){
		echo "<div class='site-message-wrapper'><div class='success-message'>" . Session::flash('success') . "</div></div>";
	} 
?>


<?php include 'inc/header.php'; ?>



<?php if($user->isLoggedIn()): ?>
			
			<?php include 'modules/toolbox.php'; ?>

		<?php else: ?>

			<?php include 'pages/about.php'; ?>

		<?php endif; ?>
	
	<div id="history-lead">
		<a href="./history.php">	
		<h3>Towards the modern museum</h3>
		<div class="dim"></div>
		</a>
	</div>

	<?php include 'modules/cities.php'; ?>

<?php include 'inc/footer.php'; ?>