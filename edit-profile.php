<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 

	if(Input::exists() && !empty($_POST)) {
		if( Token::check(Input::get('update-token')) ) {
			$validate 	= new Validate();
			
			$validation = $validate->check($_POST, array(
				'username'			=> array('required' => true, 'item' => 'A username'),
				'email'				=> array('required' => true, 'item' => 'An email '),
				'password'			=> array('required' => true, 'item' => 'Password '),
				'password_again'	=> array('required' => true, 'item' => 'Password repeat', 'matches' => 'password'),
			));


			// if valid update user deets
			if($validation->passed()) {

				$DB = DB::getInstance();


				$data =  array(
					'username'	=>	$_POST['username'],
					'password'	=>  $_POST['password'],
					'email'		=>	$_POST['email'],
				);

				foreach ($data as $index => $value) {
					$DB->update('users', $user->data()->id, array($index => $value));
				}

				Redirect::to(BASE_URL . '/profile.php');

			} else {
				$number = count($validation->errors());
				echo "<div class='site-message-wrapper errors-{$number}'>";
					
					foreach($validation->errors() as $error){
						echo $error;
					}

				echo '</div>';
			}

		}
	}




?>
	
	<title>Edit Account | Revvit</title>
</head>
<body id='main-body'>

<?php include 'inc/header.php'; ?>

	<div class="information-page">
		<header id="profile-header">
			<ul>
				<li id='profile-avatar-wrap'>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 width="414.667px" height="410.667px" viewBox="0 0 414.667 410.667" enable-background="new 0 0 414.667 410.667"
						 xml:space="preserve">
					<g>
						<circle fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" cx="208.089" cy="205.718" r="194.35"></circle>
						<path fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" d="M353.313,334.859
							c0,0-44.985-105.085-144.979-105.085c-109.61,0-145.469,105.085-145.469,105.085"></path>
						<circle fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" cx="208.089" cy="156.951" r="62.147"></circle>
					</g>
					</svg>		
				</li>
				<li class='username-wrap'>
					<h2 class="username">Hey, <?php echo escape($user->data()->username); ?></h2>
				</li>
			</ul>
		</header>
		
	<?php include 'modules/profile-header.php'; ?>
		
		<div class="edit-profile form-wrapper">
			<div class='input-wrapper' style='float:none;width:100%;'>
				<h4 class="heading"> Edit your details </h4>
			</div>

			<form method="POST">
				<div class="input-wrapper">
					<label for="username">Username</label>
					<input type="text" name="username" id="username" value="<?php echo $user->data()->username; ?>" />
				</div>
				<div class="input-wrapper">
					<label for="email">Email </label>
					<input type="email" name="email" id="email" value="<?php echo $user->data()->email; ?>"/>
				</div>
				<div class="input-wrapper">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" value="<?php echo $user->data()->password; ?>"/>
				</div>
				<div class="input-wrapper">
					<label for="password_again">Repeat password</label>
					<input type="password" name="password_again" id="password_again" value="<?php echo $user->data()->password; ?>"/>
				</div>

				<div class='input-wrapper submit-wrapper'>
					<input type="submit" value="Update">
					<input type="hidden" name="update-token" value="<?php echo Token::generate(); ?>">
				</div>
			</form>
		</div>

	</div>

<?php include 'inc/footer.php'; ?>