<!-- Tools for authors such as delete and edit post -->

<?php 
// Check for input token and check if form actually submitted
if(!empty($_POST['delete-token']) && Token::check(Input::get('delete-token'))): 
	$post_id	= $article->id;
	
	if(!empty($post_id)){
		$DB->delete('posts', array('id', '=', $post_id));

		Session::flash('success', 'You deleted '. $article->title);
		Redirect::to('index.php');
	}


	
endif; // Check for not empty

?>



<?php if($user->isLoggedin()): ?>

	<?php if($article->author_id == $user->data()->id): ?>
			<header id="author-tools">
				<h3> Author Tools </h3>
				<div class="wrapper">
					<ul>
						<li>
							<form method="POST">
								<input class='author-tool' id='delete-post' type="submit" value="Delete Post">
								<input type="hidden" name="delete-token" value='<?php echo Token::generate(); ?>'/>
							</form>
						</li>
						<li>
							<a class='author-tool' href="<?php echo BASE_URL . '/create?post_id=' . $article->id; ?>">Edit Post </a>
						</li>
					</ul>
				</div>
			</header>

	<?php endif; ?>

<?php endif; ?>