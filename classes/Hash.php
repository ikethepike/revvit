<?php

class Hash {


	// Create a random string
	public function make($length = 62){
		return bin2hex(openssl_random_pseudo_bytes($length));
	}

}