<div id='search-overlay' class='overlay-hidden'>
	
	<div class='close' id='close-overlay'>
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 width="560px" height="560px" viewBox="0 0 560 560" enable-background="new 0 0 560 560" xml:space="preserve">
			<path fill="none" stroke-width="73" stroke-miterlimit="10" d="M530,530L30,30 M530,30L30,530"></path>
		</svg>
	</div>


	<div class='inner-wrap'>
		<form action='search.php' method='GET'>
			<label for='overlay-search-box'> Search anywhere by typing </label>	
			<input name='search' id='overlay-search-box' type='text' value='' required placeholder='Glasgow...'>
		</form>
	</div>
</div>