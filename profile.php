<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 
?>

<title> <?php echo $user->data()->username; ?> | Revvit </title>

</head>
<body id='main-body'>

<?php include 'inc/header.php'; ?>

<div class="information-page">
	<header id="profile-header">
		<ul>
			<li id='profile-avatar-wrap'>
				<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 width="414.667px" height="410.667px" viewBox="0 0 414.667 410.667" enable-background="new 0 0 414.667 410.667"
					 xml:space="preserve">
				<g>
					<circle fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" cx="208.089" cy="205.718" r="194.35"></circle>
					<path fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" d="M353.313,334.859
						c0,0-44.985-105.085-144.979-105.085c-109.61,0-145.469,105.085-145.469,105.085"></path>
					<circle fill="none" stroke="#000000" stroke-width="20" stroke-miterlimit="10" cx="208.089" cy="156.951" r="62.147"></circle>
				</g>
				</svg>		
			</li>
			<li class='username-wrap'>
				<h2 class="username">Hey, <?php echo escape($user->data()->username); ?></h2>
				
			</li>
		</ul>
	</header>
	
	<?php include 'modules/profile-header.php'; ?>

	<div class="activity-log">
		<h3 class="heading"> Activity Log </h3> 
		<ul>
			<?php 
				$DB = DB::getInstance();

				$DB->get('posts', array('author_id', '=', $user->data()->id ));

				foreach($DB->results() as $result){

					$post_time 	= $result->post_time;
					$post_date 	= date_create($result->post_time);
					$formatted  = escape(date_format($post_date, 'd-m-Y'));
					$link 		= BASE_URL . "/post?id=" . $result->id;

					echo 	"<li class='result' style='background: url(".$result->featured_image . ") no-repeat center;'> <a href='{$link}'> <span class='date'> {$formatted} </span> <div class='body'> 
								 <h3>" . escape($result->title) . " </h3> 
							</div> <div class='dim'></div></a></li>";
				}

			?>


		</ul>
	</div>
</div>


<?php include 'inc/footer.php'; ?>