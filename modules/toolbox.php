<!-- Options for signed in users -->
<div class='page-wrap'>
	<div id="toolbox">
		<header>
			<h2 id="welcome-text">Welcome back, <span class='username'><?php echo $user->data()->username; ?></span></h2>
		</header>
		<ul>
			<li id='tool-create-new'>
				<a class='option-text' href='<?php echo BASE_URL; ?>/create.php'>
					<div class='bg-wrapper'>
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="418.479px" height="422.979px" viewBox="0 0 418.479 422.979" enable-background="new 0 0 418.479 422.979"
							 xml:space="preserve">
						<circle fill="none" stroke-width="33" stroke-miterlimit="10" cx="209.899" cy="212.333" r="191.808"></circle>
						<line fill="none" stroke-width="34" stroke-miterlimit="10" x1="209.899" y1="78.365" x2="209.899" y2="348.422"></line>
						<line fill="none" stroke-width="34" stroke-miterlimit="10" x1="344.927" y1="212.332" x2="74.871" y2="212.332"></line>
						</svg>
					</div>			
					 
					 <span class='action'> Create new </span>
				 </a> 

			</li>
			<li id='tool-your-account'>
				<a class='option-text' href='<?php echo BASE_URL; ?>/profile.php'>
					<div class='bg-wrapper'>
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="418.479px" height="422.979px" viewBox="0 0 418.479 422.979" enable-background="new 0 0 418.479 422.979"
							 xml:space="preserve">
						<circle fill="none" stroke-width="33" stroke-miterlimit="10" cx="209.899" cy="212.333" r="191.808"></circle>
						<g>
							<path fill="none" stroke-width="20" stroke-miterlimit="10" d="M57.499,328.793
								c0,0,42.375-111.389,151.985-111.389c99.995,0,152.17,112.229,152.17,112.229c-35.285,49.002-87.42,81.831-152.415,81.831
								C144.928,411.464,92.875,376.923,57.499,328.793z"></path>
							<circle fill="none" stroke-width="20" stroke-miterlimit="10" cx="209.24" cy="144.582" r="62.147"></circle>
						</g>
						</svg>
					</div>				
					<span class='action'> Account </span>
				</a> 
			</li>
			<li id='tool-about-revvit'>
				<a class='option-text' href='<?php echo BASE_URL; ?>/about.php'>
					<div class='bg-wrapper'>
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="418.479px" height="422.979px" viewBox="0 0 418.479 422.979" enable-background="new 0 0 418.479 422.979"
							 xml:space="preserve">
						<circle fill="none" stroke-width="33" stroke-miterlimit="10" cx="209.899" cy="212.333" r="191.808"></circle>
						<g>
							<path d="M157.103,181.722c0-34.561,24.3-56.431,53.19-56.431c29.161,0,55.352,20.25,55.352,52.651
								c0,26.19-17.82,44.551-38.881,51.031c-11.07,3.239-19.17,7.021-19.17,19.71l-0.54,14.851h-23.76v-14.851
								c0-26.19,15.39-36.45,36.181-42.931c11.34-3.24,22.14-15.12,22.14-27.81c0-17.55-15.39-28.621-31.32-28.621
								c-15.66,0-29.97,12.42-29.97,32.4H157.103z M195.443,282.163c9.72,0,17.55,7.291,17.55,17.011s-7.83,16.74-17.55,16.74
								s-17.82-7.021-17.82-16.74S185.723,282.163,195.443,282.163z"></path>
						</g>
						</svg>
					</div>	
					<span class='action'> About Revvit </span> 
				</a> 
			</li>
		</ul>
	</div>
</div>