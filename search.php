<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 

	$term = $_GET['search'];

	$DB = DB::getInstance();

	$DB->search('posts', array('title', $term));

?>
<title> <?php echo $term; ?> | Revvit </title>

</head>
<body id='main-body'>

<?php include 'inc/header.php'; ?>

	<div id='search-page' class='information-page'>
		<h2 class='heading'> <span class='gray'> You searched for: </span><?php echo $term; ?> </h2>

		<?php

		$count = $DB->count();

		echo "<div id='results'> Results: {$count} </div>";

		if($count > 0){
			echo "<ul class='search-listing'>";
			foreach ($DB->results() as $index => $post) {
				echo "<a href='" . BASE_URL . "/post?id={$post->id}'> <li class='result' style='background: url($post->featured_image) no-repeat center; background-size:cover;'><h3>". $post->title . "</h3><div class='dim'></div></li></a>";
			}
			echo "</ul>";
		} else {
			echo "no results";
		}

		?>

	</div>

<?php include 'inc/footer.php'; ?>