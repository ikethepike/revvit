<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 
?>

<title> Create New | <?php echo $site_title; ?> </title>


<!-- Initialize TinyMCE -->

<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
	tinymce.init({
	  selector: '.textarea',
	  plugins : 'autolink link image lists preview',
	});
</script>

</head>
<body id='main-body'>

<?php include 'inc/header.php'; ?>

<?php
	
	$DB = DB::getInstance();

	if(empty($_GET['post_id'])){
		$edit 		= false;
	} else {
		$edit 		= true;
		$post_id	= $_GET['post_id'];

		$DB->get('posts', array('id', '=', $post_id ));
		$article = $DB->first();

		$title 		= $article->title;
		$intro 		= $article->introduction;
		$x_values	= $article->x_values;
		$y_values	= $article->y_values;
		$content 	= $article->content;
		$feat_image = $article->featured_image;
	}

	if(Input::exists() && !empty($_POST)){

		if(Token::check(Input::get('create-token')) ) {				

			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'post-title'	=> array(
					'item'		=> 'Title',
					'table'		=> 'posts',
					'required'	=> true,
					'min'		=> 3,
					'max'		=> 70,
					'alpha'		=> true,
				),
				'post-content'	=> array(
					'item'		=> 'Content',
					'table'		=> 'posts',
					'required'	=> true,
					'alpha'		=> true,
				),
				'post-introduction'	=> array(
					'item'		=> 'introduction',
					'table'		=> 'posts',
					'required'	=> true,
					'alpha'		=> true,
				),

			));

			if($validation->passed()) {
				

				$data = array(
					'author_id' 		=> $user->data()->id,
					'title'     		=> $_POST['post-title'],
					'introduction'		=> $_POST['post-introduction'],
					'content' 			=> $_POST['post-content'],
					'x_values'			=> $_POST['x-values'],
					'y_values'			=> $_POST['y-values'],
				);

				if(!$edit){
					
					$DB->get('uploads', array('hash', '=', $_POST['image-hash']) );
					$image = $DB->first();

					$data['featured_image'] = $image->url;
					$DB->insert('posts', $data);

				} else {

					$data['featured_image'] = $feat_image;

					// Temp fix for only single field update working

					foreach ($data as $field_name => $value) {
						$DB->update('posts', $post_id, array(
								$field_name	=>	$value
							)
						);
					}

					Redirect::to(BASE_URL . "/post.php?id=". $post_id);



				}

			} else {
				$number = count($validation->errors());
				echo "<div class='site-message-wrapper errors-{$number}'>";
				foreach ($validation->errors() as $error ) {
					echo  $error;
				}
				echo "</div>";
			}

		} // Token Check

	} // Input exists


?>



<?php if($user->isLoggedin()) : ?>

<div class="information-page" id='post-creation-wrapper'>
	<header class="create-header">
		<h2> City Editor </h2>
		<span>
			You are creating a new city post, the post will attempt to populate with extra information and media.
		</span>	
	</header>
	
<form method="POST">
	<div class='body'>
		<div class="form-wrapper">
			
			<?php include 'modules/file-upload.php'; ?>
			

			<div class='input-wrapper'>
				<label for='post-title'> Title </label>
				<input type="text" name="post-title" id="post-title" value='<?php echo ($edit) ? $title : Input::get("post-title"); ?>'>
			</div>
			
			<div class='input-wrapper'>
				<label for='post-introduction'> Introduction </label>
				<textarea class='textarea' name="post-introduction" id="post-introduction" cols="30" rows="10"><?php echo ($edit) ? $intro : Input::get("post-introduction"); ?></textarea>
			</div>

			<div class='input-wrapper'>
				<label for='post-content'> Content </label>
				<textarea class='textarea' name="post-content" id="post-content" cols="30" rows="10"><?php echo ($edit) ? $content : Input::get("post-content"); ?></textarea>
			</div>

			<!-- Expenditure widget -->
			<?php include 'modules/data-entry.php'; ?>


			<div class='input-wrapper'>
				<input type="submit" value="Submit" class='black-button'/>
				<input type='hidden' name='create-token' id='create-token' value='<?php echo Token::generate(); ?>'/>
			</div>

		</div> <!-- Form Wrapper End -->
	</div> <!-- Body end -->

</form>
	

</div>

<?php else: ?>
	
	<div class='information-page'>
		<h2 style="font-size:3em;display:block;text-align:center;padding:3em 0;"> You have to be logged in to create a post </h2>
	</div>

<?php endif; ?>

<?php include 'inc/footer.php'; ?>