<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 

	$post_id 	= $_GET['id'];
		
	$DB = DB::getInstance();
	$DB->get('posts', array('id', '=', $post_id ));
	$article = $DB->first();

?>


	<title><?php echo escape($article->title); ?> | Revvit </title>

</head>
<body id='main-body'>

<!-- Cop out Jquery Flickr querying -->

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){

   	$.getJSON("http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?",
   	{
     tags: '<?php echo $article->title; ?>',
     tagmode: "any",
     format: "json"
   	},
   	function(data) {
     	$.each(data.items, function(i,item){
     		$('#masthead-side-gutter').append('<div style="background:url(' + item.media.m+') no-repeat center;"></div>');
       	if ( i == 2 ) return false;
     });
   });
 });
</script>

<?php include 'inc/header.php'; ?>

<?php if(!empty($post_id)):
	
	include 'modules/author_tools.php';

	include 'modules/article.php';

	else: 

	Redirect::to('404.php');
	
endif; ?>


<?php include 'inc/footer.php'; ?>