
<?php

		if(Input::exists() && !empty($_POST)){

			if(Token::check(Input::get('register-token')) ) {				

				$validate = new Validate();
				$validation = $validate->check($_POST, array(
					'username'			=> array(
						'item'		=> 'Username',
						'table'		=> 'users',
						'required'	=> true,
						'min'		=> 3,
						'max'		=> 30,
						'alpha'		=> true,
						'unique'	=> true
					),
					'password'			=> array(
						'item'		=> 'Password',
						'required'	=> true,
						'min'		=> 6,
						'max'		=> 50,
						'unique'	=> false
					),
					'password_repeat'	=> array(
						'item'		=> 'Password',
						'required'	=> true,
						'min'		=> 6,
						'max'		=> 50,
						'matches'	=> 'password'
					),
					'email'				=> array(
						'item'		=> 'Email',
						'table'		=> 'users',
						'unique'	=> true,
						'required'	=> true,
						'min'		=> 5,
						'max'		=> 32
					)
				));

				if($validation->passed()) {

					$user = new User();

					$password_hash	= Input::get('password');
					$activation 	= input::get('username') + uniqid();
					$activation		= md5( $activation );

					try {

						$user->create(array(
							'username'		=> Input::get('username'),
							'password'		=> $password_hash,
							'activation'	=> $activation,
							'email'			=> Input::get('email') 
						));


					} catch (Exception $e) {
						echo $e->getMessage();
					}

					Session::flash('success', 'You have successfully registered!');
					Redirect::to('index.php');

				} else {
					$number = count($validation->errors());
					echo "<div class='site-message-wrapper errors-{$number}'>";
					foreach ($validation->errors() as $error ) {
						echo  $error;
					}
					echo "</div>";
				}

			} // Token Check

		} // Input exists

?>


<div id='register-form-wrapper'>
	<form action='' method="POST">
		<div class='field'>
			<label for='username'> Username </label>
			<input type='text' name='username' id='username' value='<?php echo Input::get("username"); ?>'>
		</div>

		<div class='field'>
			<label for='email'> Email </label>
			<input type='email' name='email' id='email' value='<?php echo Input::get("email"); ?>'>
		</div>

		<div class="field">
			<label for='password'> Password </label>
			<input type='password' name='password' id='password'>
		</div>

		<div class="field">
			<label for='password_repeat'> Password </label>
			<input type='password' name='password_repeat' value='' id='password_repeat'>
		</div>

		<div class='submit-wrapper'>
			<input type='hidden' name='register-token' value='<?php echo Token::generate(); ?>'>
			<input type="submit" class='submit-button' id='register-button' value='Register'>
		</div>

	</form>
</div>