<?php 
	require_once('core/init.php');
	include 'inc/head.php'; 
	$title = 'Towards the modern museum';
?>


<title> <?php echo $title; ?> </title>

</head>
<body id='main-body'>

<!-- History page custom masthead with scrolly background things -->
<header id="history-masthead">
	
	<h2><?php echo $title; ?></h2>
	
	<!-- Fixed bg money -->
	<div id="scroll-overlay"></div> 
	
	<!-- Improve legibility -->
	<div id="dim"></div>
</header>


<!-- Custom Header -->

<?php include 'inc/sidemenu.php'; ?>
<div id='site-header-wrapper'>
	<header id='site-header' class='menu-inactive '>
		<nav>
			<div id='logo-wrapper'>
				<ul>
					
					<li id='main-logo'>
						<a href='<?php echo BASE_URL; ?>'>
							<h1 title='Go home'> <?php echo $site_title; ?> </h1>
						</a>
					</li>

					<li id='main-menu'>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class='hamburger'><g><path d="M3,18h18v-2H3V18z M3,13h18v-2H3V13z M3,6v2h18V6H3z"></path></g></svg>
					</li>
				</ul>
			</div>
		</nav>

	</header>
</div>


<!-- Main wrapper element for history content  -->
<div id='history-page-wrapper'>
	<div class="text-block">
		<div class="main-flow full-span">
			<p id='first-paragraph'>
				 The modern museum is a deeply political entity. Charting its rise from the private rooms of collectors, spaces filled with curios and knick-knacks, to the contemporary museums and galleries of today, we find a history rife with political and economic motivation. What follows is an abridged history and examination of the rise of the public museum in Europe. Focusing upon the social and political issues concomitant with its rise. 
			</p>
		</div>
	</div>
	<div class="media-block" id='wunderkammer'>
		<div class="dim"></div>
		<h3>The Wunderkammer</h3>
		<small class="caption"><a href='http://stefanobessoni.blog.tiscali.it/2013/08/23/wunderkammer/?doing_wp_cron'> Image credit Stefano Bessoni </a></small>
	</div>
	<div class="text-block">
		<div class="main-flow">
			<p>While private art collections had existed for centuries, the notion of a quasi-public exhibition space was still largely unheard of. Against this backdrop, the Wunderkammer or <em> Cabinet of Curiosities </em> of the 16th century may be seen to have been the earliest predecessor to the modern museum.<sup>1</sup> Shared between both of these was a desire to attempt to categorize, archive and tell stories of wonder from the natural world. </p>

		<p>
			In practice these stories often took a morbid or macabre guise, as the Wunderkammers paraded little more than a freak show of disfigured animals or skeletons. Perhaps most famous amongst the Wunderkammers was that of famed Dutch anatomist, <a href="http://publicdomainreview.org/2014/03/05/frederik-ruysch-the-artist-of-death/"> Frederik Ruysch </a>. His morbid collection became famous around the world, resulting in it being bought by Tsar Alexander the Great <sup>2</sup>. 
		</p>

		<p>
			However, Wunderkammers were privately owned and operated. Moreover, they were often set in small rooms and spaces, or relegated to small sideshows at festivals or fairs. They simply could not host the amount of public that a modern museum could. 
		</p>
		</div>
		<div class="media-gutter"></div>
	</div>
	<div class="media-block" id='kunstkammer'>
		<small class="caption"> Teniers, David the Younger, Archduke Leopold Wilhelm in his Gallery, c. 1647, Oil on copper, 106 x 129 cm, Museo del Prado, Madrid</small>
		<h3>Kunstkammer</h3>
	</div>
	<div class="text-block">
		<div class="main-flow">
			<p>The kunstkammer was an incremental step from the Wunderkammer. No longer was the focus upon <em> "wondrous" </em> objects of the world, but rather upon fine art. Yet, still was firmly in the hands of private collectors, meaning that the Kunstkammers were largely exclusive to the bourgeois and aristocratic classes. </p>

			<p>
				The importance of the kunstkammer in the evolution of the modern museum can not be understated. They presented the private collection as a public venue, creating a clear logical trajectory to the modern museum. A connection that is further strengthened by the eventual transformation of many early UK kunstkammers into real museums. Notably, the Ashmolean Museum housed in the University of Oxford was one of the first kunstkammers in the United Kingdom.<sup>3</sup> 
			</p>
		</div>
		<div class="media-gutter">
			<img src="http://i.telegraph.co.uk/multimedia/archive/01511/ashmolean_1511913c.jpg" alt="Picture of the Ashmolean">
		</div>
	</div>
	
	<div><h3 id='dawn-heading'>The Dawn of the Modern Museum</h3></div>
	<div class="text-block">
		<div class="main-flow center">
			<p>
				The first truly <a href='http://www.newworldencyclopedia.org/entry/Museum'>public and modern museum in Europe was the Louvre </a>. Opened in 1792, the Louvre with its immense, imperial halls of the enormous structure were filled with grandiose works of fiction and history, works by national and international artists. It was built as a testament to France and its culture, a nationalistic theme  further contretized by the French monarchy showcasing the immense collection of works that they had amassed across centuries.   
			</p>
			<img src="img/louvre.jpg" alt="Image of the Louvre">
			<p>
				The Louvre would swell in size following the Napoleonic campaigns. The conquest through Europe saw cities raided and their priceless works of art and <a href="http://www.theguardian.com/artanddesign/2014/nov/13/10-most-notorious-looted-artworks-nazis-napoleon"> culture being transported back to Paris. </a> These stolen works therefore became a very physical testament to the French power, strength and in a sense cultural dominance in mainland Europe. Napoleon sought to inspire nationalist fervor in France and support for his costly foreign campaigns. Following his subsequent defeat in 1815, France saw many of the works return to their original owners. Yet the original notion - to plunder cities of their works to foment national pride at home is a trope that sadly repeats itself again and again. Most notably perhaps under the third reich who saw stolen <a href="http://www.vanityfair.com/news/2014/04/degenerate-art-cornelius-gurlitt-munich-apartment">650,000 works shipped to Germany. </a>
			</p>
		</div>
	</div>

	<div class="media-block" id='expo'>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/qizg7a-52jg" frameborder="0" allowfullscreen></iframe>
	</div>
	<div class="text-block">
		<div class="main-flow">
			<p>
				Napoleon awakened a sense of nationalism in museums and cultural institutions throughout Europe. This would later culminate in events such as the <b> World Fair </b> (Exposition Universelle), where nations would vye to host the prestigious international event to showcase their national progress in industry, science and the arts. 
			</p>

			<p>
				The expositions were lavish and costly events, giving rise to some of the most noticable cultural landmarks in modern history such as the Eiffel tower in Paris.(The 1889 Exposition Universelle in Paris is estimated to have cost 41,500,000 Francs) <sup>Page 736, 7</sup> Yet, for all the breadth and scale of the expositions, the art that they actually accepted and displayed was not as expansive. 
			</p>
			<p>
				Art was predominantly selected on its reflection of either national or moral virtue. This formalization of art was in part due to the prevalence of national art academies (e.g Salon in France, Royal Academy in London). Artists who defied these stricts artistic moulds were excluded, this was the case with Gustave Courbet who in 1855 applied to exhibit at the exposition universelle but was rejected. Left with no recourse, Courbet created his own Pavilion of Realism, thereby defying the primacy of the formal art institutions. <sup>5</sup>
			</p>
		
			<p>
				While a small act of defiance such as this was not enough to meaningfully affect the formalized structure of art and its exhibition. It did set an important precedent, which would be repeated in 1874 when the impressionists set up their own, independent exhibition apart from the Salon or formal art institution. <sup>6</sup> The power over the museum began to slowly and grudgingly be rest away from political and national leaders. 
			</p>

		</div>
		<div class="media-gutter">
			<h4>Expositions 1851 - 1900</h4>
			<ul class='bullet-points'>
				<li>1851 London</li>
				<li>1853 New York </li>
				<li>1862 London</li>
				<li>1876 Philadelphia</li>
				<li>1889 Paris</li>
				<li>1893 Chicago</li>
				<li>1900 Paris</li>
			</ul>
		</div>
	</div>
	<div class="media-block" id='modern-museum'>
		<div class="dim"></div>
		<h3>Contemporary museums</h3>
	</div>

	<div class="text-block">
		<div class="main-flow">
			<p>
				Today, most European museums espouse values of independence and conservation. The goal of national galleries and museums has shifted from attempting to propound national culture - to trying to preserve it.
			</p>
		</div>
		<div class="media-gutter"></div>
	</div>
	



	<!-- Bibliography -->
	<div class='references'>
	<header>
		<h3>References</h3>
	</header>
		<ol>
			<li>
				<a href="https://www.metmuseum.org/toah/hd/kuns/hd_kuns.htm">
				Koeppe, Wolfram. "Collecting for the Kunstkammer | Essay | Heilbrunn Timeline of Art History | The Metropolitan Museum of Art." The Met's Heilbrunn Timeline of Art History. Metropolitan Museum of Art, 1 Oct. 2002. Web. 21 Mar. 2016. <https://www.metmuseum.org/toah/hd/kuns/hd_kuns.htm>.
				</a>
			</li>
			<li> 
				<a href="http://publicdomainreview.org/2014/03/05/frederik-ruysch-the-artist-of-death/"> Kooijmans, Luuc, Mr. "Frederik Ruysch: The Artist of Death." The Public Domain Review. The Public Domain Review, 12 Mar. 2014. Web. 21 Mar. 2016. <http://publicdomainreview.org/2014/03/05/frederik-ruysch-the-artist-of-death/>.</a>
			</li>
			<li><a href="http://www.ashmolean.org/about/historyandfuture/">
				Ashmolean Museum. "About: History & Future." Ashmolean Museum. Ashmolean Museum, 08 July 2006. Web. 21 Mar. 2016. <http://www.ashmolean.org/about/historyandfuture/>.
			</a></li>
			<li><a href="http://www.vanityfair.com/news/2014/04/degenerate-art-cornelius-gurlitt-munich-apartment">Shoumatoff, Alex. "The Devil and the Art Dealer." Vanity Fair. Vanity Fair, 1 Apr. 2014. Web. 21 Mar. 2016. <http%3A%2F%2Fwww.vanityfair.com%2Fnews%2F2014%2F04%2Fdegenerate-art-cornelius-gurlitt-munich-apartment>.</a></li>

			<li><a href="http://www.metmuseum.org/toah/hd/gust/hd_gust.htm">Galitz, Kathryn Calley. "Gustave Courbet (1819–1877) | Essay | Heilbrunn Timeline of Art History | The Metropolitan Museum of Art." The Met's Heilbrunn Timeline of Art History. Metropolitan Museum of Art, 1 May 2009. Web. 21 Mar. 2016. <http://www.metmuseum.org/toah/hd/gust/hd_gust.htm>.</a></li>
			<li><a href="https://www.metmuseum.org/toah/hd/imml/hd_imml.htm">Samu, Margaret. "Impressionism: Art and Modernity | Essay | Heilbrunn Timeline of Art History | The Metropolitan Museum of Art." The Met's Heilbrunn Timeline of Art History. Metropolitan Museum of Art, 1 Oct. 2004. Web. 21 Mar. 2016. <https://www.metmuseum.org/toah/hd/imml/hd_imml.htm>.</a></li>
			<li>
				Northrop, Henry Davenport, and Nancy Huston. Banks. The World's Fair as Seen in One Hundred Days: Containing a Complete History of the World's Columbian Exposition. Philadelphia: Ariel Book, 1893. Print.
			</li>
		</ol>
		
	</div>
</div>


<?php include 'inc/footer.php'; ?>