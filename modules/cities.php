<!-- City Listing -->

<?php $DB = DB::getInstance(); ?>

<?php $cities = $DB->table('posts'); 

// Check if empty

if(!empty($cities)): ?> 

	<div class="cities">
		<h2 class="heading"> Cities </h2>
		<ul>
			<?php foreach($cities as $index => $city): ?>
			
					<li class="result" style="background: url(<?php echo $city['featured_image']; ?>) no-repeat center; background-size:cover;">
						<a href="<?php echo BASE_URL . "/post.php?id=" . $city['id']; ?>">
							<h3 title='<?php echo $city['title']; ?>'><?php echo $city['title']; ?></h3>
							<div class="dim"></div>
						</a>
					</li>

			<?php endforeach; ?>
		</ul>
	</div>

<?php endif; ?>